#![no_std]
#![no_main]

use panic_halt as _;

#[allow(unused_imports)]
use nrf52840_hal as hal;

#[cortex_m_rt::entry]
fn main() -> ! {
    loop {}
}
